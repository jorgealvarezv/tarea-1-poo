--------------------------------------Versión: Final.-------------------------------------------

Ramo: ELO-329
Profesor: Werner Creixell

integrantes: -Jorge Alvarez			Rol:	201721065-65
			
Asunto: Tarea 1

Dentro de esta tarea se encuentran 4 carpetas, cada una correspondiente
a cada etapa descrita dentro de las instrucciones de esta.

Dentro de la carpeta correspondiente a cada etapa existen 3 tipos de archivos,
los *.java correspondientes a cada clase.

El otro tipo de archivo que existe es el "configuration.txt", que es un archivo de texto
en donde se agregan las características de los dispositivos domoticos según el siguiente formato:
<#_de_cortinas> <#_de_lámparas> <#_controles_cortinas> <#_controles_lámparas>
<alfa0> <length0> <canal0> … <alfaN_1> <lengthN_1> <canalN_1> // rapidez angular [rad/s], largo
de la tela y canal de la primera hasta n-ésima nortina
<canal0>…..<canalL_1> // canales de la primera a la L-ésima lámpara
<canal0>. … <canalM_1> // canales del primer hasta el m-ésimo control de cortina.
<canal0>…. <canalK_1> // canales del primer hasta el k-ésimo control de lámpara.
<T [s] > <C | L> <#channel> <Command> // Tiempo entero y en segundos, Cortina o Lámpara, comando

Por último existe el archivo Makefile, este contiene lo necesario para compilar y
ejecutar cada etapa.

Para compilar la etapa deseada, por favor ingrese a la carpeta correspondiente
Ejemplo: cd Stage1

Luego con el comando "make" se compilará la etapa correspondiente
Ejemplo: make

Para correr el código con el "configuration.txt" base, basta con usar el comando "make run"
Ejemplo: make run

Para correr el código con otro "configuration.txt", ocupar el comando como ' make run FILE="archivo.txt"
Ejemplo make run FILE="ejemplo.txt"

Para limpiar la carpeta de archivos .class, ocupe el comando "make clean".

Al finalizar el código usted verá que se creó o sobreescribio el archivo salida.csv,
este corresponde al archivo de salida donde se encuentran los datos solicitados en cada etapa.