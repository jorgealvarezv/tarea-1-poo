public class Lamp extends DomoticDevice{
    public Lamp (int channel){
        super(channel,nextId);
        state = 0;
        r=0;
        g=0;
        b=0;
    }
    {
        nextId=nextId++;
    }
    public int getChannel(){
        return channel;
    }
    public void changePowerState(){
      if(state == 0)
      {
          state = 1;
          r=255;
          g=255;
          b=255;
      }
      else
      {
          state = 0;
      }
    }
    public String getHeader()
    {
        int id_lamp = getId();
        return "L"+id_lamp+"R"+"\t"+"L"+id_lamp+"G"+"\t"+"L"+id_lamp+"B";
    }
    public String toString(){
        if (state == 1)
            return ""+r+"\t"+g+"\t"+b;
        else
            return "0\t0\t0";
    }
    private int channel;
    private short r,g,b;
    private int state;
    private static int nextId=0;
}
