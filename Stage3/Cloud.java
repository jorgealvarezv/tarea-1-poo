import java.util.ArrayList;

public class Cloud {
    public Cloud() {
        lamps = new ArrayList<DomoticDevice>();
        rollerShades = new ArrayList<DomoticDevice>();
    }
    public void addLamp(Lamp l){
        lamps.add(l);
    }
    public void addRollerShade(RollerShade rs){
        rollerShades.add(rs);
    }
    public void advanceTime(double delta){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            rs.advanceTime(delta);
        }
    }
    private DomoticDevice getDomoticDeviceAtChannel( ArrayList<DomoticDevice> devices, int channel){
        DomoticDevice dd1 =null;
        for (DomoticDevice dd: devices)
        {
            if(dd.getChannel() == channel)
            {
                dd1=dd;
            }
        }
        return dd1;
    }
    public void changeLampPowerState(int channel){
        Lamp l = (Lamp) getDomoticDeviceAtChannel(lamps,channel);
        l.changePowerState();
        return;
    }
    public void changeLampRedUp(int channel)
    {
        Lamp l = (Lamp) getDomoticDeviceAtChannel(lamps,channel);
        l.redUp();
        return;
    }
    public void changeLampRedDown(int channel)
    {
        Lamp l = (Lamp) getDomoticDeviceAtChannel(lamps,channel);
        l.redDown();
        return;
    }
    public void changeLampGreenUp(int channel)
    {
        Lamp l = (Lamp) getDomoticDeviceAtChannel(lamps,channel);
        l.greenUp();
        return;
    }
    public void changeLampGreenDown(int channel)
    {
        Lamp l = (Lamp) getDomoticDeviceAtChannel(lamps,channel);
        l.greenDown();
        return;
    }
    public void changeLampBlueUp(int channel)
    {
        Lamp l = (Lamp) getDomoticDeviceAtChannel(lamps,channel);
        l.blueUp();
        return;
    }
    public void changeLampBlueDown(int channel)
    {
        Lamp l = (Lamp) getDomoticDeviceAtChannel(lamps,channel);
        l.blueDown();
        return;
    }
    public void startShadeUp(int channel){
        RollerShade rs = (RollerShade) getDomoticDeviceAtChannel(rollerShades,channel);
        rs.startUp();
    }
    public void startShadeDown(int channel){
        RollerShade rs = (RollerShade) getDomoticDeviceAtChannel(rollerShades,channel);
        rs.startDown();
    }
    public void stopShade(int channel){
        RollerShade rs = (RollerShade) getDomoticDeviceAtChannel(rollerShades,channel);
        rs.stop();
    }
    public String getHeaders(){
        String header = "";
        for (DomoticDevice  rs: rollerShades)
            header += rs.getHeader()+"\t";
        for (DomoticDevice l: lamps)
            header += l.getHeader()+"\t";
        return header;
    }
    public String getState(){
        String state = "";
        for (DomoticDevice  rs: rollerShades)
            state += rs.toString()+"\t";
        for (DomoticDevice l: lamps)
            state += l.toString()+"\t";
        return state;
    }
    private ArrayList<DomoticDevice> lamps;
    private ArrayList<DomoticDevice> rollerShades;
}
