import java.io.PrintStream;
import java.util.Scanner;
import java.lang.*;
public class Operator {
    public Operator(LampControl lampControl,ShadeControl rsControl,Cloud c){
        cloud = c;
        this.rsControl = rsControl;
        this.lampControl=lampControl;
    }
    /*public void addShadeControl(ShadeControl sc){
        // ???
    }*/
    public void executeCommands(Scanner in, PrintStream out){
        out.println("Time\t" + cloud.getHeaders());
        while(in.hasNextInt()){
            int commandTime=in.nextInt();
            while (time < commandTime) {
                out.println(time+"\t"+cloud.getState());
                cloud.advanceTime(delta);
                time+=delta;
                time=Math.round(time*100.0)/100.0;
            }
            String device=in.next();
            if (device.equals("C"))
            {
                int channel = in.nextInt();
                String command=in.next();
                if (channel == rsControl.getChannel())
                {
                    switch (command.charAt(0))
                    {
                        case 'D':
                            rsControl.startDown();
                            break;
                        case 'U':
                            rsControl.startUp();
                            break;
                        case 'S':
                            rsControl.stop();
                            break;
                        default: out.println("Unexpected command:" + command);
                            System.exit(-1);
                    }
                }

            }
            else if (device.equals("L"))
            {
                int channel = in.nextInt();
                String command= in.next();
                if(channel == lampControl.getChannel())
                {
                    switch (command.charAt(0))
                    {
                        case 'P':
                            lampControl.pressPower();
                            break;
                        case 'R':
                            command = in.next();
                            if(command.equals("U"))
                            {
                                lampControl.lampRedUp();
                                break;
                            }
                            else if(command.equals("D")){
                                lampControl.lampRedDown();
                                break;
                            }
                            else
                            {
                                out.println("Unexpected command:" + command);
                                System.exit(-1);
                            }
                        case 'G':
                            command = in.next();
                            if(command.equals("U"))
                            {
                                lampControl.lampGreenUp();
                                break;
                            }
                            else if(command.equals("D")){
                                lampControl.lampGreenDown();
                                break;
                            }
                            else
                            {
                                out.println("Unexpected command:" + command);
                                System.exit(-1);
                            }
                        case 'B':
                            command = in.next();
                            if(command.equals("U"))
                            {
                                lampControl.lampBlueUp();
                                break;
                            }
                            else if(command.equals("D")){
                                lampControl.lampBlueDown();
                                break;
                            }
                            else
                            {
                                out.println("Unexpected command:" + command);
                                System.exit(-1);
                            }
                        default: out.println("Unexpected command:" + command);
                            System.exit(-1);
                    }
                }
            }
            else
            {
                out.println("Unexpected device:" + device);
                System.exit(-1);
            }

        }
        out.println(time+"\t"+cloud.getState());
    }
    private double time=0;
    private ShadeControl rsControl;
    private Cloud cloud;
    private final double delta=0.1;
    private LampControl lampControl;
}
