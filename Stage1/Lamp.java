public class Lamp {
    public Lamp (int channel){
        state = 0;
        r=0;
        g=0;
        b=0;
        this.channel = channel;
    }
    {
        id=nextId++;
    }
    public int getChannel(){
        return channel;
    }
    public void changePowerState(){
      if(state == 0)
      {
          state = 1;
          r=255;
          g=255;
          b=255;
      }
      else
      {
          state = 0;
      }
    }
    public String getHeader(){
       return "L"+id+"R"+"\t"+"L"+id+"G"+"\t"+"L"+id+"B";
    }
    public String toString(){
        if (state == 1)
            return ""+r+"\t"+g+"\t"+b;
        else
            return "0\t0\t0";
    }
    private int channel;
    private short r,g,b;
    private int state;
    private final int id;  // to conform lamp's header
    private static int nextId=0;
}
