import java.util.ArrayList;

public class Cloud {
    public Cloud() {
        lamps = new ArrayList<Lamp>();
    }
    public void addLamp(Lamp l){
        lamps.add(l);
    }
    public Lamp getLampAtChannel( int channel)
    {
        Lamp l1=null;
        for (Lamp l: lamps)
        {
            if(l.getChannel() == channel)
            {
                l1=l;
            }
        }
        return l1;
    }
    public void changeLampPowerState(int channel)
    {
        getLampAtChannel(channel).changePowerState();
        return;
    }
    public String getHeaders(){
        String header = "";
        for (Lamp l: lamps)
            header += l.getHeader();
        return header;
    }
    public String getState(){
        String state = "";
        for (Lamp l: lamps)
            state += l.toString();
        return state;
    }
    private ArrayList<Lamp> lamps; // getting ready for next stages
}
