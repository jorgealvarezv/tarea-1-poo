import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Stage4 {
    public static void main(String [] args) throws IOException {
        if (args.length != 1) {
            System.out.println("Usage: java Stage1 <configurationFile.txt>");
            System.exit(-1);
        }
        Scanner in = new Scanner(new File(args[0]));
        //System.out.println("File: " + args[0]);
        Cloud cloud = new Cloud();
        // reading <#_de_cortinas> <#_de_lámparas> <#_controles_cortinas> <#_controles_lámparas>
        int numRollerShades = in.nextInt();
        int numLamps = in.nextInt();
        int numShadeControls = in.nextInt();
        int numLampsControl = in.nextInt(); // skip number of lamp's controls
        // read <alfa0> <length0> <canal0> … <alfaN_1> <lengthN_1> <canalN_1>
        double alpha = in.nextDouble();
        double maxLength = in.nextDouble();
        int channel = in.nextInt();
        for (int i = 0; i <numRollerShades ; i++) {
            RollerShade rollerShade = new RollerShade(channel, alpha, maxLength);
            cloud.addRollerShade(rollerShade);
        }
        int channelLamp = in.nextInt();
        for (int i = 0; i < numLamps; i++) {
            Lamp lamp = new Lamp(channel);
            cloud.addLamp(lamp);
        }

        //System.out.println("next int:"+ in.nextInt()); // skip lamp's channels
        // creating just one roller shade's control at <canal0>
        channel = in.nextInt();
        ShadeControl shadeControl = new ShadeControl(channel, cloud);
        channel = in.nextInt(); // skipping creation of lamp's control at <canal0>
        LampControl lampControl = new LampControl(channel, cloud);
        Operator operator = new Operator(lampControl,shadeControl, cloud);
        operator.executeCommands(in, System.out);
    }
}
