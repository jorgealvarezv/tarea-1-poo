public class Lamp extends DomoticDevice{
    public Lamp (int channel) {
        super(channel, nextId);
        state = 0;
        r = 0;
        g = 0;
        b = 0;
        nextId++;

    }
    public int getChannel(){
        return channel;
    }
    public void changePowerState(){
      if(state == 0)
      {
          state = 1;
          if(r==0 && g==0 && b==0)
          {
            r=255;
            g=255;
            b=255;
          }

      }
      else
      {
          state = 0;
      }
    }
    public void redUp(){
        if(r+10<255)
            r+=10;
        else
            r=255;
    }
    public void redDown(){
        if(r-10>0)
            r-=10;
        else
            r=0;
    }
    public void greenUp(){
        if(g+10<255)
            g+=10;
        else
            g=255;
    }
    public void greenDown(){
        if(g-10>0)
            g-=10;
        else
            g=0;
    }
    public void blueUp(){
        if(b+10<255)
            b+=10;
        else
            b=255;
    }
    public void blueDown(){
        if(b-10>0)
            b-=10;
        else
            b=0;
    }
    public String getHeader()
    {
        int id_lamp = getId();
        //System.out.println(id_lamp);
        return "L"+id_lamp+"R"+"\t"+"L"+id_lamp+"G"+"\t"+"L"+id_lamp+"B";
    }
    public String toString(){
        if (state == 1)
            return ""+r+"\t"+g+"\t"+b;
        else
            return "0\t0\t0";
    }
    private int channel;
    private static int r,g,b;
    private int state;
    private static int nextId=0;
}
