import java.util.ArrayList;

public class Cloud {
    public Cloud() {
        lamps = new ArrayList<DomoticDevice>();
        rollerShades = new ArrayList<DomoticDevice>();
    }
    public void addLamp(Lamp l){
        lamps.add(l);
    }
    public void addRollerShade(RollerShade rs){
        rollerShades.add(rs);
    }
    public void advanceTime(double delta){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            rs.advanceTime(delta);
        }
    }
    private ArrayList<DomoticDevice> getDomoticDeviceAtChannel( ArrayList<DomoticDevice> devices, int channel){
        DomoticDevice dd1 =null;
        domoticsDevices = new ArrayList<DomoticDevice>();

        for (DomoticDevice dd: devices)

        {
            if(dd.getChannel() == channel)
            {
                domoticsDevices.add(dd);
            }
        }
        return domoticsDevices;
    }
    public void changeLampPowerState(int channel){
        domoticsDevices = getDomoticDeviceAtChannel(lamps,channel);
        Lamp l = null;
        for (DomoticDevice dd: domoticsDevices)
        {
            l = (Lamp) dd;
            l.changePowerState();
        }
        return;
    }

    public void changeLampRedUp(int channel)
    {
        domoticsDevices = getDomoticDeviceAtChannel(lamps,channel);
        Lamp l = null;
        for (DomoticDevice dd: domoticsDevices)
        {
            l = (Lamp) dd;
            l.redUp();
        }
        return;
    }
    public void changeLampRedDown(int channel)
    {
        domoticsDevices = getDomoticDeviceAtChannel(lamps,channel);
        Lamp l = null;
        for (DomoticDevice dd: domoticsDevices)
        {
            l = (Lamp) dd;
            l.redDown();
        }
        return;

    }
    public void changeLampGreenUp(int channel)
    {
        domoticsDevices = getDomoticDeviceAtChannel(lamps,channel);
        Lamp l = null;
        for (DomoticDevice dd: domoticsDevices)
        {
            l = (Lamp) dd;
            l.greenUp();
        }
        return;
    }
    public void changeLampGreenDown(int channel)
    {
        domoticsDevices = getDomoticDeviceAtChannel(lamps,channel);
        Lamp l = null;
        for (DomoticDevice dd: domoticsDevices)
        {
            l = (Lamp) dd;
            l.greenDown();
        }
        return;
    }
    public void changeLampBlueUp(int channel)
    {
        domoticsDevices = getDomoticDeviceAtChannel(lamps,channel);
        Lamp l = null;
        for (DomoticDevice dd: domoticsDevices)
        {
            l = (Lamp) dd;
            l.blueUp();
        }
        return;
    }
    public void changeLampBlueDown(int channel)
    {
        domoticsDevices = getDomoticDeviceAtChannel(lamps,channel);
        Lamp l = null;
        for (DomoticDevice dd: domoticsDevices)
        {
            l = (Lamp) dd;
            l.blueDown();
        }
        return;
    }
    public void startShadeUp(int channel){
        domoticsDevices = getDomoticDeviceAtChannel(rollerShades,channel);
        RollerShade rs = null;
        for (DomoticDevice dd: domoticsDevices)
        {
            rs = (RollerShade) dd;
            rs.startUp();
        }
        return;
    }
    public void startShadeDown(int channel){
        domoticsDevices = getDomoticDeviceAtChannel(rollerShades,channel);
        RollerShade rs = null;
        for (DomoticDevice dd: domoticsDevices)
        {
            rs = (RollerShade) dd;
            rs.startDown();
        }
        return;
    }
    public void stopShade(int channel){
        domoticsDevices = getDomoticDeviceAtChannel(rollerShades,channel);
        RollerShade rs = null;
        for (DomoticDevice dd: domoticsDevices)
        {
            rs = (RollerShade) dd;
            rs.stop();
        }
        return;
    }
    public String getHeaders(){
        String header = "";
        for (DomoticDevice  rs: rollerShades)
            header += rs.getHeader()+"\t";
        for (DomoticDevice l: lamps)
            header += l.getHeader()+"\t";
        return header;
    }
    public String getState(){
        String state = "";
        for (DomoticDevice  rs: rollerShades)
            state += rs.toString()+"\t";
        for (DomoticDevice l: lamps)
            state += l.toString()+"\t";
        return state;
    }
    private ArrayList<DomoticDevice> lamps;
    private ArrayList<DomoticDevice> rollerShades;
    private ArrayList<DomoticDevice> domoticsDevices;
}
