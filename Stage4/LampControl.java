public class LampControl {
    public LampControl(int channel, Cloud c){
        this.channel = channel;
        cloud = c;
    }
    public void pressPower(){
        cloud.changeLampPowerState(channel);
    }
    public void lampRedUp(){
        cloud.changeLampRedUp(channel);
    }
    public void lampRedDown(){
        cloud.changeLampRedDown(channel);
    }
    public void lampGreenUp(){
        cloud.changeLampGreenUp(channel);
    }
    public void lampGreenDown(){
        cloud.changeLampGreenDown(channel);
    }
    public void lampBlueUp(){
        cloud.changeLampBlueUp(channel);
    }
    public void lampBlueDown(){
        cloud.changeLampBlueDown(channel);
    }

    public int getChannel(){
        return channel;
    }
    private Cloud cloud;
    private int channel;
}
