public class RollerShade extends DomoticDevice {
    public RollerShade (int channel, double alpha, double length) {
        super(nextId,channel);
        motor = new Motor(alpha);
        MaxShadeLength = length;
        nextId=nextId++;
        openness = 0;
    }

    public void startUp(){
        motor.turnUp();
    }
    public void startDown(){
        motor.turnDown();
    }
    public void stop(){
        motor.stop();
    }
    public void advanceTime(double delta){
        motor.advanceTime(delta);
    }
    public String getHeader(){
        String s = "RS" + getId();
        return s;
    }
    public String toString(){
        String s = String.valueOf(Math.round(openness/MaxShadeLength*100));
        return s;
    }
    private class Motor {  //nested class, Motor is only used within RollerShade.
        public Motor (double a){
            alpha = a;
            state = 0;
        }
        public void turnUp(){
            state = 1;
        }
        public void turnDown(){state = -1;}
        public void stop(){state = 0;}
        public void advanceTime(double delta){
            double increment = alpha*delta*RADIUS;
            switch (state) {

                case S: break;
                case D:
                    if(openness>increment)
                    {
                        openness=openness-increment;
                        break;
                    }
                    else
                    {
                        openness=0;
                        break;
                    }

                case U:
                    if(openness+increment<MaxShadeLength)
                    {
                        openness=openness+increment;
                        break;
                    }
                    else
                    {
                        openness=MaxShadeLength;
                        break;
                    }

            }
        }
        private double alpha;
        private int state;
        private final double RADIUS=0.04;
    }

    private Motor motor;
    private double length;
    private final double MaxShadeLength;
    private static int nextId=0;
    private double openness;
    private final int U = 1;
    private final int D = -1;
    private final int S = 0;
}
